package Auftrag2;

import java.io.FileWriter;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import javafx.util.Builder;

/* Auftrag 2) Erstellen Sie folgende book2.json:
{
	"titel": "Java ist auch eine Insel",
	"jahr": 1998,
	"preis": 29.95,
	"autor": "Christian Ullenboom"
}
*/
public class WriteBookDataJason {

	public static void main(String[] args) {

		Buch b1 = new Buch("Java ist auch eine Insel", 1998, 29.95, "Christian Ullenboom");

		JsonObjectBuilder bookBuilder = Json.createObjectBuilder();
		bookBuilder.add("titel", b1.getTitel());
		bookBuilder.add("jahr", b1.getJahr());
		bookBuilder.add("preis", b1.getPreis());
		bookBuilder.add("autor", b1.getAutor());
		JsonObject jo = bookBuilder.build();
		
		try {
			FileWriter fw = new FileWriter("book2.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo); 

			fw.close();
			jw.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
