package Auftrag1;

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData1 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag1/buchhandlung.xml"
			// Add your code here

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("buchhandlung1.xml");

			NodeList titelList = doc.getElementsByTagName("titel");
			Node titelNode = titelList.item(0);

			System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
