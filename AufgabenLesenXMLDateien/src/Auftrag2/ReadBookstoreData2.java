package Auftrag2;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

/* Arbeitsauftrag:  Lesen Sie den Titel und den Autor des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                        titel:  Java ist auch eine Insel 
 *                        autor:  Max Mustermann 
 */

public class ReadBookstoreData2 {

	public static void main(String[] args) {

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("buchhandlung2.xml");

			NodeList titelList = doc.getElementsByTagName("titel");
			NodeList autorList = doc.getElementsByTagName("autor");
			Node titelNode = titelList.item(0);
			Node autorNode = autorList.item(0);

			System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());
			System.out.println(autorNode.getNodeName() + ": " + autorNode.getTextContent());
			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			// Add your code here

		} catch (Exception e) {
		}

	}

}
