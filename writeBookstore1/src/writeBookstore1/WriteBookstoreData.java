package writeBookstore1;

import java.io.File;

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

public class WriteBookstoreData {

	public static void main(String[] args) {

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element buecher = doc.createElement("buecher");
			doc.appendChild(buecher); // verkn�pft buecher mit doc

			Element buch = doc.createElement("buch");
			buecher.appendChild(buch); // verkn�pft buch mit buecher

			Element titel = doc.createElement("titel");
			buch.appendChild(titel); // verkn�pft titel mit buch

			titel.appendChild(doc.createTextNode("Java ist auch eine Insel"));

			// Vorbereitung XMLFile schreiben 

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			
			DOMSource xmlSource = new DOMSource(doc);
			StreamResult outputTarget = new StreamResult(new File ("Buchhandlung.xml"));
			
			transformer.transform(xmlSource, outputTarget);

		} catch (Exception ex) {
			ex.printStackTrace();

		}

	}
}