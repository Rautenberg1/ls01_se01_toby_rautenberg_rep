/*
 *  {
 *  	"titel": "XQuery Kick Start",
// *  	"verlag": {
 * 				"name": "Sams",
 * 				"Ort": "Indianapolis"
 * 		},
 * 		"autoren":
 * 			"James McGover",
 * 			"Per Bothner",
 * 			"Kurt Cagle",
 * 			"James Linn",
 */
import java.io.*;
import javax.json.*;
import javax.json.spi.*;

public class WriteBookDataJason {

	public static void main(String[] args) {

		JsonObjectBuilder bookBuilder = Json.createObjectBuilder();
		bookBuilder.add("titel", "XQuery Kick Start");

		JsonObjectBuilder verlagBuilder =Json.createObjectBuilder();
		verlagBuilder.add("name", "Sams");
		verlagBuilder.add("ort", "Indianapolis");
		bookBuilder.add("verlag", verlagBuilder);
	
		JsonArrayBuilder autorlisteBuilder = Json.createArrayBuilder();
		autorlisteBuilder.add("James McGovern");
		autorlisteBuilder.add("Per Bothner");
		autorlisteBuilder.add("Kurt Cagle");
		autorlisteBuilder.add("James Linn");
		bookBuilder.add("autoren", autorlisteBuilder);
		

	
		JsonObject jo = bookBuilder.build();

		try {
			FileWriter fw = new FileWriter("book.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		
		}

	}

}
