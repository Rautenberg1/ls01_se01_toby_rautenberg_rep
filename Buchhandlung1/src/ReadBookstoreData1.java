import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* Arbeitsauftrag:  Lesen Sie den Titel des Buches "Java ist auch eine Insel" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  Ausgabe soll wie folgt aussehen:
 *                     titel:  Java ist auch eine Insel                  
 */

public class ReadBookstoreData1 {

	public static void main(String[] args) {

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.parse("Buchhandlung.xml");

//Buch 0	
			System.out.println("Buch 0");
			Element TitelElem = (Element) doc.getElementsByTagName("Titel").item(0);
			System.out.println(TitelElem.getTextContent());
			Element AutorElem = (Element) doc.getElementsByTagName("Autor").item(0);
			System.out.println(AutorElem.getTextContent());
			Element VerlagElem = (Element) doc.getElementsByTagName("Verlag").item(0);
			System.out.println(VerlagElem.getTextContent());
//Buch 1	
			System.out.println("Buch 1");
			Element TitelElem1 = (Element) doc.getElementsByTagName("Titel").item(1);
			System.out.println(TitelElem1.getTextContent());
			Element AutorElem1 = (Element) doc.getElementsByTagName("Autor").item(0);
			System.out.println(AutorElem1.getTextContent());
			Element VerlagElem1 = (Element) doc.getElementsByTagName("Verlag").item(0);
			System.out.println(VerlagElem.getTextContent());
		
		} catch (Exception e) {
			System.out.println("Error: Datei konnte nicht gelesen werden!");
		}

	}

}
