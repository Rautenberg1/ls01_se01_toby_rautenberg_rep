package Auftrag3;

 /* Arbeitsauftrag:  Lesen Sie alle Angaben des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                    titel:  Java ist auch eine Insel   
 *					  vorname:  Christian 
 *                    nachname:  Ullenboom 
 */

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData3 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("buchhandlung3.xml");

			NodeList titelList = doc.getElementsByTagName("titel");
			NodeList vornameList = doc.getElementsByTagName("vorname");
			NodeList nachnameList = doc.getElementsByTagName("nachname");
			Node titelNode = titelList.item(0);  
			Node vornameNode = vornameList.item(0);
			Node nachnameNode = nachnameList.item(0);

			System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());
			System.out.println(vornameNode.getNodeName() + ": " + vornameNode.getTextContent());
			System.out.println(nachnameNode.getNodeName() + ": " + nachnameNode.getTextContent());
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
