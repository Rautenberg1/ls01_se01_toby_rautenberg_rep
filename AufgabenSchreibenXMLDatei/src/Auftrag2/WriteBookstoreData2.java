package Auftrag2;
/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben 
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml" 
 * 					und sichern Sie es als XML in eine Datei 
 * 					mit dem Filename "buchhandlung.xml".
 * 	
 *               
 */

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteBookstoreData2 {

	public static void main(String[] args) {

		try {
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newDefaultInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();
			
			Element buchhandlung = doc.createElement("buchhandlung");
			doc.appendChild(buchhandlung); //Verkn�pft Buchhandlung mit Document
			
			Element buch = doc.createElement("buch");
			buchhandlung.appendChild(buch); //Verkn�pft Buch mit Buchhandlung
			
			Element titel = doc.createElement("titel");
			buch.appendChild(titel); //Verkn�pft Titel mit Buch
			
			Element autor = doc.createElement("autor");
			titel.appendChild(autor); //Verkn�pft autor mit titel
			
			titel.appendChild(doc.createTextNode("Java ist auch eine Insel"));
			autor.appendChild(doc.createTextNode("Christian Ullenboom"));
			
			// XML File Schreiben
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			
			DOMSource xmlSource = new DOMSource(doc);
			StreamResult outputTarget = new StreamResult(new File("Vorgabe_f�r_Ausgabedatei2.xml"));
			
			transformer.transform(xmlSource, outputTarget);
			
			
		}	catch(Exception ex) {
			ex.printStackTrace();
		}

	}

}
