/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Toby Rautenberg >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
    int   anzahlSterne = 100-300 ;
    
    // Wie viele Einwohner hat Berlin?
    int    bewohnerBerlin = 3669491 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int   alterTage = 5933 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int   gewichtKilogramm =   200000  ;
    
    // Schreiben Sie auf, wie viele km das gr��te Land er Erde hat?
    int   flaecheGroessteLand = 17100000 ;
    
    // Wie gro� ist das kleinste Land der Erde?
    float   flaecheKleinsteLand = 0.44f ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten:	"+ anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne:	" + anzahlSterne);
    System.out.println("Einwohner Zahl von Berlin:	" + bewohnerBerlin);
    System.out.println("Alter in Tage:	" + alterTage);
    System.out.println("Gewicht vom schwersten Tier:	" + gewichtKilogramm);
    System.out.println("Gr��e vom gr��tes Land:	" + flaecheGroessteLand);
    System.out.println("Gr��e vom kleinstes Land:	" + flaecheKleinsteLand);
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

