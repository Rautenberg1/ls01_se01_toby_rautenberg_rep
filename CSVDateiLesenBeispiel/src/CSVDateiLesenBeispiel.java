import java.io.BufferedReader;
import java.io.FileReader;

public class CSVDateiLesenBeispiel {

	public static void main(String[] args) {

		try {

			FileReader fr = new FileReader("Kopie von artikelliste.csv");
			BufferedReader reader = new BufferedReader(fr);

			String Zeile = reader.readLine();
			
			while (Zeile != null) {
			System.out.println(Zeile);
			Zeile = reader.readLine();
			}
			

		} catch (Exception ex) {
			System.out.println("Error: Datei konnte nicht gelesen werden");

		}

	}
}