import java.util.Scanner;

public class Eingabebeispiele {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben sie bitte eine Zahl ein:");
		
		int zahl1 = myScanner.nextInt();
		
		System.out.println("zahl :"+ zahl1); 
		
		System.out.println("Geben sie bitte ihren Vornamen ein:");
		
		String vorname = myScanner.next();
			
		System.out.println("Vorname :" + vorname);
		
		System.out.println("Geben sie bitte ein Symbol ein:");
		
		char buchstabe = myScanner.next().charAt(0);
		
		System.out.println("Buchstabe :" + buchstabe);
		
		
		
	}
	

}
