package Auftrag1;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben 
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml" 
 * 					und sichern Sie es als XML in eine Datei 
 * 					mit dem Filename "buchhandlung.xml".
 * 	
 *               
 */

public class WriteBookstoreData1 {

	public static void main(String[] args) {

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newDefaultInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element buchhandlung = doc.createElement("buchhandlung");
			doc.appendChild(buchhandlung); // Hier wird Buchhandlung mit Document verkn�pft

			Element buch = doc.createElement("buch");
			buchhandlung.appendChild(buch); // Hier wird Buch mit Buchhandlung verkn�pft

			Element titel = doc.createElement("titel");
			buch.appendChild(titel); // Hier wird Titel mit Buch verkn�pft

			titel.appendChild(doc.createTextNode("Java ist auch eine Insel"));

			// XML File Schreiben

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			DOMSource xmlSource = new DOMSource(doc);
			StreamResult outputTarget = new StreamResult(new File("Vorgabe_f�r_Ausgabedatei.xml"));

			transformer.transform(xmlSource, outputTarget);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
